<?php

namespace Drupal\ips\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Ips type entities.
 */
interface IpsTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
