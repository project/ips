<?php

namespace Drupal\ips\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ips entities.
 *
 * @ingroup ips
 */
class IpsDeleteForm extends ContentEntityDeleteForm {


}
